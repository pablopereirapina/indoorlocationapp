package com.riatec.demo.indoorlocation.Generation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.riatec.demo.indoorlocation.R;

public class SelectFloorQRGenerationActivity extends AppCompatActivity {
    private Button floor0;
    private Button floor1;
    private Button floor2;
    private Button floor3;
    private Button floor4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_floor_qrgeneration);

        floor0 = (Button) findViewById(R.id.floor0Button);
        floor1 = (Button) findViewById(R.id.floor1Button);
        floor2 = (Button) findViewById(R.id.floor2Button);
        floor3 = (Button) findViewById(R.id.floor3Button);
        floor4 = (Button) findViewById(R.id.floor4Button);

        floor0.setOnClickListener(onClickListener);
        floor1.setOnClickListener(onClickListener);
        floor2.setOnClickListener(onClickListener);
        floor3.setOnClickListener(onClickListener);
        floor4.setOnClickListener(onClickListener);
    }
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            Intent i = new Intent(SelectFloorQRGenerationActivity.this, SelectFloorPointQRGenerationActivity.class);
            switch(v.getId()){
                case R.id.floor0Button:
                    i.putExtra("floor",0);
                    startActivity(i);
                    break;
                case R.id.floor1Button:
                    i.putExtra("floor",1);
                    startActivity(i);
                    break;
                case R.id.floor2Button:
                    i.putExtra("floor",2);
                    startActivity(i);
                    break;
                case R.id.floor3Button:
                    i.putExtra("floor",3);
                    startActivity(i);
                    break;
                case R.id.floor4Button:
                    i.putExtra("floor",4);
                    startActivity(i);
                    break;
            }

        }
    };
}
