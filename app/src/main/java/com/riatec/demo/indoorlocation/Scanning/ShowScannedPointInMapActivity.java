package com.riatec.demo.indoorlocation.Scanning;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.riatec.demo.indoorlocation.MainActivity;
import com.riatec.demo.indoorlocation.R;

public class ShowScannedPointInMapActivity extends AppCompatActivity {
    ImageView image;
    Button homeButton;
    ImageView marker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_scanned_point_in_map);

        Intent prevIntent =  getIntent();
        String scanResult = prevIntent.getStringExtra(getString(R.string.scan_result_extra));

        String[] scanValues = scanResult.split(";");

        int floor = Integer.parseInt(scanValues[0]);
        int xValue = Integer.parseInt(scanValues[1]);
        int yValue = Integer.parseInt(scanValues[2]);

        homeButton = (Button) findViewById(R.id.homeButton);
        image = (ImageView) findViewById(R.id.imageView);
        marker = (ImageView)findViewById(R.id.markerImage);

        setHomeButtonListener();
        drawPointInMap(floor, xValue, yValue);
    }

    private void setHomeButtonListener() {
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowScannedPointInMapActivity.this.finish();
            }
        });
    }

    private void drawPointInMap(int floor, int xValue, int yValue) {
        setImageByFloor(floor);
        setMarkerPosition(xValue, yValue);
    }

    private void setMarkerPosition(int xValue, int yValue) {
        marker.setX(xValue);
        marker.setY(yValue);
    }

    private void setImageByFloor(int floor) {
        switch (floor){
            case 0:
                image.setImageResource(R.drawable.floor0);
                break;
            case 1:
                image.setImageResource(R.drawable.floor1);
                break;
            case 2:
                image.setImageResource(R.drawable.floor2);
                break;
            case 3:
                image.setImageResource(R.drawable.floor3);
                break;
            case 4:
                image.setImageResource(R.drawable.floor4);
                break;
            default:
                image.setImageResource(R.drawable.floor0);
        }
    }
}
