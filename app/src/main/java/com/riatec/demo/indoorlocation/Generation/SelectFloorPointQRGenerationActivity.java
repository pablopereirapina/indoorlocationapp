package com.riatec.demo.indoorlocation.Generation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.riatec.demo.indoorlocation.R;

public class SelectFloorPointQRGenerationActivity extends AppCompatActivity {
    private ImageView iv;
    private ImageView marker;
    private Button generate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_floor_point_qrgeneration);

        iv = (ImageView) findViewById(R.id.floorImage);
        marker = (ImageView)findViewById(R.id.markerImage);
        generate = (Button) findViewById(R.id.generateQRButton);

        Intent i = getIntent();
        final int idFloor = i.getIntExtra("floor",0);

        switch (idFloor){
            case 0:
                iv.setImageResource(R.drawable.floor0);
                break;
            case 1:
                iv.setImageResource(R.drawable.floor1);
                break;
            case 2:
                iv.setImageResource(R.drawable.floor2);
                break;
            case 3:
                iv.setImageResource(R.drawable.floor3);
                break;
            case 4:
                iv.setImageResource(R.drawable.floor4);
                break;
        }

        View myView = findViewById(R.id.view);
        myView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                marker.setX(event.getX());
                marker.setY(event.getY());
                return true;
            }
        });

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent("com.google.zxing.client.android.ENCODE");
                i.putExtra("ENCODE_FORMAT","QR_CODE");
                i.putExtra("ENCODE_TYPE","TEXT_TYPE");
                i.putExtra("ENCODE_DATA",idFloor+";"+(int)marker.getX()+";"+(int)marker.getY());
                i.putExtra("ENCODE_SHOW_CONTENTS",true);
                startActivity(i);
            }
        });

    }
}
