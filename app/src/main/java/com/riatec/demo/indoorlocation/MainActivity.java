package com.riatec.demo.indoorlocation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.riatec.demo.indoorlocation.Generation.SelectFloorQRGenerationActivity;
import com.riatec.demo.indoorlocation.Scanning.ShowScannedPointInMapActivity;

public class MainActivity extends AppCompatActivity {
    Button generateButton;
    Button scanButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generateButton = (Button) findViewById(R.id.generateButton);
        scanButton = (Button) findViewById(R.id.scanButton);

        generateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, SelectFloorQRGenerationActivity.class);
                startActivity(i);
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.google.zxing.client.android.SCAN");
                i.putExtra("SCAN_MODE","QR_CODE_MODE");
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //finishes activity when code is scanned
                startActivityForResult(i,0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0){
            if(resultCode==RESULT_OK){
                String contents = data.getStringExtra(getString(R.string.scan_result_extra));
                Intent i = new Intent(MainActivity.this, ShowScannedPointInMapActivity.class);
                i.putExtra(getString(R.string.scan_result_extra),contents);
                startActivity(i);
            }
        }
    }
}
